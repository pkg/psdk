psdk (0.2023.0) apertis; urgency=medium

  * Refresh the automatically detected licensing information
  * Update copyright report

 -- Walter Lozano <walter.lozano@collabora.com>  Mon, 12 Jun 2023 16:20:23 -0300

psdk (0.2021.3) apertis; urgency=medium

  * Add debian/apertis/copyright
  * Add debian/apertis/lintian
  * Override lintian tag no-manual-page
  * Switch deprecated extra priority to optional
  * Switch to debhelper-compat (= 13)

 -- Dylan Aïssi <dylan.aissi@collabora.com>  Mon, 23 Jan 2023 12:06:16 +0100

psdk (0.2021.2) apertis; urgency=medium

  * Switch to single partition
    Partition split is no more requested
  * Add docker storage to persistent partition

 -- Frédéric Danis <frederic.danis@collabora.com>  Mon, 16 Aug 2021 15:34:34 +0200

psdk (0.2021.1) apertis; urgency=medium

  * Add fdisk dependency

 -- Frédéric Danis <frederic.danis@collabora.com>  Thu, 05 Aug 2021 17:36:00 +0200

psdk (0.2021.0) apertis; urgency=medium

  [ Peter Senna Tschudin ]
  * Update REAME.md to match psdk interactive mode
  * psdk: Code cleanup

  [ Emanuele Aina ]
  * debian/apertis/component: Set to sdk

 -- Emanuele Aina <emanuele.aina@collabora.com>  Tue, 02 Mar 2021 14:51:15 +0000

psdk (0.2019~dev0.2) apertis; urgency=medium

  * Detects disks automatically instead of relying on user to find the correct
    device node
  * Calling psdk without arguments starts a simple GUI
  * Automatically moves /etc/cntlm.conf to persistent storage when
    initializing the disk
  * Includes a systemd service to scan for configuration files on persistent
    storage during boot

 -- Peter Senna Tschudin <peter.senna@collabora.com>  Mon, 17 Jun 2019 11:27:35 +0200

psdk (0.2019~dev0.1) apertis; urgency=medium

  * Initial release.

 -- Peter Senna Tschudin <peter.senna@collabora.com>  Sat, 08 Jun 2019 19:58:24 +0200
