# Introduction

psdk is tool that helps the developer to decouple user files from the Apertis
SDK. The idea is to use a second disk that can be attached to more than one SDK
to store:
 - /home
 - /opt
 - /etc/cntlm.conf
 - Other configuration files selected by the developer

The background information about the design of psdk can be found at:
	https://designs.apertis.org/private/latest/maintaining-workspace-across-sdk-updates.html

Calling psdk without arguments will start the interactive mode. However it has the following options:

	user@apertis:~$ psdk -h
	usage: psdk [-h] [-e] [-s] [-c] [-i] [path]
	
	positional arguments:
	  path             Path to a configuration file e.g. /etc/apt/sources.list
	
	optional arguments:
	  -h, --help       show this help message and exit
	
	Configuration files management:
	  -e, --etc        Move a configuration file to the persistent storage. Use
	                   this option once for each file you want to move to the
	                   persistent storage. Usage: psdk -e /etc/configuration.file.
	                   A backup of the original file will be at
	                   /etc/configuration.file.PSDK. Use the original path (e.g.
	                   /etc/configuration.file) to make changes to a file after
	                   moving it to the persistent storage.
	  -s, --scan       Scan persistent storage for configuration files and
	                   configures the SDK to use each configuration file found.
	
	Persistent disk management:
	  -c, --configure  Configure this SDK to use use an already initialized disk
	                   for persistent workspace.
	  -i, --init       Initialize a new disk for persistent workspace. Files from
	                   /home and from /opt will be copied from this SDK. After
	                   initializing the disk psdk will configure this SDK to use
	                   the persistent disk.

# Using psdk to upgrade to a new version of the SDK

The SDK is distributed as a VirtualBox image, and developers make changes to
adjust the SDK to their needs. These changes include installing tools,
libraries, changing system configuration files, and adding content to their
workspace. There is one VirtualBox image for each version of the SDK, and
before psdk a version upgrade required each developer to manually migrate their
SDK customization to the new version.

psdk automates the tasks of moving the developer customization (/home, /opt,
and /etc/cntlm.conf) to a second disk and configuring the SDK to use the second
disk.

## Upgrading to a new SDK using psdk interactive mode

First step is to prepare the persistent disk on the old SDK. Start by adding a
second disk to the old SDK on VirtualBox. This disk should be big enough to
host the contents of /home and /opt: We recommend using more than 40GiB and no
less than 20GiB.  As we will use dynamically allocated VDI images the unused
space does not consume disk space on the host, so if in doubt, create a larger
disk. Step by step instructions of how to add a second disk are on the section
*Detailed instructions on upgrading to a new SDK using psdk*.

After adding the second disk, start the old SDK and click on the psdk icon:
Applications -> System -> *Persistent Disk Setup*. psdk will open in
interactive mode, and will guide you trough the processes of initializing the
new disk and configuring the SDK to use it.

After psdk is done, it will reboot the SDK. Wait for the reboot to complete,
and turn off the old SDK.

On VirtualBox add the persistent disk you configured on the old SDK to the new
SDK. Detailed instructions on how to do this are on step 5 of section
*Detailed instructions on upgrading to a new SDK using psdk*.

Start the new SDK and click on the psdk icon: Applications -> System ->
*Persistent Disk Setup*. psdk will open in interactive mode, and will guide you
trough the processes of configuring the SDK to use the persistent disk.

## Detailed instructions on upgrading to a new SDK using psdk

ATTENTION: Do not use VirtualBox *Shareable hard disks* expert feature. ext4
file system is not prepared to handle this feature, and using it will lead to
file system corruption and data loss. VirtualBox documentation on [Special
Write Modes](https://www.virtualbox.org/manual/ch05.html#hdimagewrites) has
more information.

First step is to prepare the persistent disk on the old SDK. Start by adding a
second disk to the old SDK. This disk should be big enough to host the contents
of /home and /opt: We recommend using more than 40GiB and no less than 20GiB. As
we will use dynamically allocated VDI images the unused space does not consume
disk space on the host, so if in doubt, create a larger disk.

1 - Add a second disk to the old SDK using VirtualBox (used v6.0.8 for instructions)
- Go to Settings of the old SDK Virtual Machine
- Go to Storage
- Select Controller SATA
- Click on "Adds a new storage attachment" icon and then "Add Hard Disk"
- Choose Create new disk
- Choose VDI (VirtualBox Disk Image) and click Next
- Choose Dynamically allocated and click Next
- Choose the folder where to save the VDI file and name the disk as
  apertis-persistent-sdk.vdi. For the disk size we recommend no less
  than 20GiB and preferably more than 40GiB.
- Click on Create

2 - Start the old SDK virtual machine and call psdk:
- Applications -> System -> *Persistent Disk Setup*
- psdk will open in interactive mode, and will guide you trough the processes
  of initializing the new disk and configuring the SDK to use it.
- When it is done, psdk will reboot the SDK. Wait for the reboot to complete.

2(a) - Moving more configuration files to the persistent storage

This is an optional step. By default psdk will move /etc/cntlm.conf to the
persistent storage, but you can also move other configuration files.

- Before starting make sure that step 2 was completed and that the SDK has
  rebooted after configuring the persistent storage.
- For each configuration file you want on persistent storage open a terminal
  and call `psdk -e /etc/file`. As an example:

    user@oldSDK:~$ psdk -e /etc/xfce4/defaults.list
    Configuration file moved to persistent storage. From now on, changes made to
    /etc/xfce4/defaults.list will be saved on the persistent storage.
    
Here is what psdk -e does:
- Copy the contents of the configuration file to the persistent storage
  dereferencing symbolic links.
- Rename the configuration file to add the .PSDK extension. So in our
  example the backup will be at /etc/xfce4/defaults.list.PSDK.
- Create a symbolic link from the persistent storage to the
  configuration file.

After call `psdk -e /etc/xfce4/defaults.list`, you can continue to use the
configuration file /etc/xfce4/defaults.list normally. Any changes made will be
saved on the persistent storage.

3 - Power off the old SDK
After you are done with the persistent storage, power off the old SDK.

4 - Add the persistent disk to the new SDK. On VirtualBox:
- Go to Settings of the new SDK Virtual Machine
- Go to Storage
- Select Controller SATA
- Click on "Adds a new storage attachment" icon and then "Add Hard Disk"
- Click on Choose existing disk
- You will find apertis-persistent-sdk.vdi under Attached. Click on it
  and then on Choose.
- Click on OK

5 - Configure the new SDK to use the persistent disk:
Start the new SDK and click on the psdk icon: Applications -> System ->
*Persistent Disk Setup*. psdk will open in interactive mode, and will guide you
trough the processes of configuring the SDK to use the persistent disk.

# Where are the old files and folders?
The old /home, /opt and /var/lib/docker directories are accessible from /run/media/root/. A
backup copy of configuration files that were moved to the persistent are kept
on /etc with the extension .PSDK.

# Recovering the SDK
If you accidentally delete the persistent disk of an SDK, the SDK will not
longer boot. To fix an SDK that does not book due missing persistent disk:
 - Stop the SDK bootloader from automatically booting. To do so you need to
   press space a few times. When you succeed the bootloader will show you a menu
   with two options: Apertis and Reboot to Firmware Interface
 - Press `e` to edit the Apertis entry.
 - The initrd and root variables are important. Do not change the content of
   these, but delete everything else.
 - Add init=/bin/sh at the end of the line
 - Press enter to boot
 - Remount the file system with write permissions: mount -o remount,rw /
 - Fixing the SDK involves editing /etc/fstab, removing .mount files from
   /etc/systemd/system, and fixing files on /etc that are links to the persistent
   storage.
 - Call: sync
 - Force-off the SDK

Please note that even if the SDK will boot after these changes, no content from
the persistent SDK will be available as the disk is no longer available.
